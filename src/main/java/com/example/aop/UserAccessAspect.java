package com.example.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Order(2)
@Component
@Aspect
public class UserAccessAspect {
    @Pointcut(value = "@annotation(com.example.aop.UserAccess)")
    public void access() {
    }

    @Before("access() ")
    public void doBefore(JoinPoint joinPoint) {
        System.out.println("second before");
    }

    @Around("@annotation(userAccess)")
    public Object around(ProceedingJoinPoint pjp,UserAccess userAccess){
        //获取注解里的值
        System.out.println("second around:"+userAccess.desc());
        try {
            return pjp.proceed();//执行目标方法
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            return null;
        }

    }
}
