package com.example.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;

/**
 * 创建一个 aspect 切面类
 * 日志切面
 *
 * @Aspect 作用是把当前类标识为一个切面供容器读取
 *
 * @Before 标识一个前置增强方法，相当于BeforeAdvice的功能
 *
 * @AfterReturning 后置增强，相当于AfterReturningAdvice，方法退出时执行
 *
 * @AfterThrowing 异常抛出增强，相当于ThrowsAdvice
 *
 * @After  final增强，不管是抛出异常或者正常退出都会执行
 *
 * @Around 环绕增强，相当于MethodInterceptor
 */
@Order(1)
@Aspect
@Component
public class aop {
    @Pointcut("execution(public * com.example.controller.*.*(..))")
    public void webLog() {
    }

    @Before("webLog()")
    public void doBefore(JoinPoint joinPoint) {
        // 接收到请求，记录请求内容
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //记录下请求内容
        System.out.println("URL:" + request.getRequestURI().toString());
        System.out.println("HTTP_METHOD:" + request.getMethod());
        System.out.println("IP:" + request.getRemoteAddr());
        System.out.println("CLASS_METHOND:" + joinPoint.getSignature().getDeclaringTypeName() + "." + joinPoint.getSignature().getName());
        System.out.println("ARGS:" + Arrays.toString(joinPoint.getArgs()));
    }
    @AfterReturning(returning = "ret",pointcut = "webLog()")
    public void doAfterReturning(Object ret){
        // 处理完请求，返回内容
        System.out.println("方法的返回值："+ret);
    }

    //后置异常通知
    @AfterThrowing("webLog()")
    public void throwss(JoinPoint jp){
        System.out.println("方法异常是执行......");
    }

    //后置最终通知。final增强，不管是抛出异常或者正常退出都会执行
    @After("webLog()")
    public void after(JoinPoint jp){
        System.out.println("方法最后执行....");
    }

    //环绕通知，环绕增强，相当于 MethodInterceptor
    @Around("webLog()")
    public Object arround(ProceedingJoinPoint pjp){
        System.out.println("方法环绕 start ......");

        try {
            Object o = pjp.proceed();
            System.out.println("方法环绕proceed,结果："+o);
            return o;
        } catch (Throwable e) {
            e.printStackTrace();
            return null;
        }
    }
}
