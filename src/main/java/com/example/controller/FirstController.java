package com.example.controller;

import com.example.aop.UserAccess;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FirstController {
    // http://localhost:8080/first
    @RequestMapping("/first")
    public Object first() {
        return "first controller";
    }

    // http://localhost:8080/first/id=3
    @RequestMapping("/first/{id}")
    public Object first2(@PathVariable("id") String id) {
        return "first controller" + id;
    }

    // http://localhost:8080/doError
    @RequestMapping("/doError")
    public Object error() {
        return 1 / 0;
    }

    // http://localhost:8080/second
    @RequestMapping("/second")
    @UserAccess(desc = "second")
    public Object second() {
        return "second controller";
    }

}
